#!/usr/bin/python3
# -*- coding: utf-8 -*-

# CifradoSAESI: El programa de cifrado y descifrado con SAESI.
# Copyright (C) 2021  free4fun (Mauricio Sosa Giri) <free4fun@riseup.net>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import math;
import random;
DEBUG = True; #Poner en True para ver mensajes de diagnostico, etc.

dictionary = {
    'A' : 'M',
    'B' : '.',
    'C' : 'H',
    'D' : 'D',
    'E' : 'Q',
    'F' : 'L',
    'G' : 'B',
    'H' : 'P',
    'I' : 'G',
    'J' : 'T',
    'K' : 'Z',
    'L' : 'V',
    'M' : 'A',
    'N' : '!',
    'Ñ' : 'J',
    'O' : 'R',
    'P' : 'Ñ',
    'Q' : 'S',
    'R' : 'O',
    'S' : '?',
    'T' : 'I',
    'U' : 'K',
    'V' : ':',
    'W' : 'C',
    'X' : 'X',
    'Y' : 'F',
    'Z' : 'N',
    '.' : 'E',
    ':' : 'W',
    '?' : 'Y',
    '!' : ' ',
    ' ' : 'U' };

binarySymbols = {
    'A' : bin(int('00000',2)),
    'B' : bin(int('00001',2)),
    'C' : bin(int('00010',2)),
    'D' : bin(int('00011',2)),
    'E' : bin(int('00100',2)),
    'F' : bin(int('00101',2)),
    'G' : bin(int('00110',2)),
    'H' : bin(int('00111',2)),
    'I' : bin(int('01000',2)),
    'J' : bin(int('01001',2)),
    'K' : bin(int('01010',2)),
    'L' : bin(int('01011',2)),
    'M' : bin(int('01100',2)),
    'N' : bin(int('01101',2)),
    'Ñ' : bin(int('01110',2)),
    'O' : bin(int('01111',2)),
    'P' : bin(int('10000',2)),
    'Q' : bin(int('10001',2)),
    'R' : bin(int('10010',2)),
    'S' : bin(int('10011',2)),
    'T' : bin(int('10100',2)),
    'U' : bin(int('10101',2)),
    'V' : bin(int('10110',2)),
    'W' : bin(int('10111',2)),
    'X' : bin(int('11000',2)),
    'Y' : bin(int('11001',2)),
    'Z' : bin(int('11010',2)),
    '.' : bin(int('11011',2)),
    ':' : bin(int('11100',2)),
    '?' : bin(int('11101',2)),
    '!' : bin(int('11110',2)),
    ' ' : bin(int('11111',2)) }

def menu():
    print("El equipo [BUGS] de TICripto te da la bienvenida al programa de cifrado/descifrado de SAES.");
    print("Ingresa la letra 'c' para cifrar con cifrado SAES.");
    print("Ingresa la letra 'd' para descifrar con cifrado SAES.");
    print("Ingresa las letras 'ci' para descifrar con cifrado SAESI.");
    print("Ingresa las letras 'di' para descifrar con cifrado SAESI.");
    print("Ingresa la palabra 'salir' para cerrar el programa.");

def getMatrix(dimentions, text):
    matrix = [[None]*dimentions]*dimentions;
    pos = 0;
    for r in range(dimentions):
        col = [None]*dimentions;
        for c in range(dimentions):
            col[c] = text[pos];
            pos += 1;
        matrix[r] = col;
    return matrix;

def doSubstitution(matrix):
    for r in range(len(matrix)):
        for c in range(len(matrix)):
            matrix[r][c] = dictionary[matrix[r][c]];
    return matrix;

def doInverseSubstitution(matrix):
    for r in range(len(matrix)):
        for c in range(len(matrix)):
            matrix[r][c] = getKeyFromDict(matrix[r][c], dictionary);
    return matrix;

def doCorrimientoFilasIzq(matrix):
    matrix = transpose(matrix);
    for r in range(len(matrix)):
        if (r >= 1):
            matrix[r] = rotateLeft(matrix[r],r);
    matrix = transpose(matrix);
    return matrix;

def doCorrimientoFilasDer(matrix):
    matrix = transpose(matrix);
    for r in range(len(matrix)):
        if (r >= 1):
            matrix[r] = rotateRight(matrix[r],r);
    matrix = transpose(matrix);
    return matrix;

def doTransformacionColumnas(matrix):
    matrixTrans = matrix;
    matrix = transpose(matrix);
    for r in range(len(matrix)):
        for c in range(len(matrix)):
            col = [];
            for i in range(len(matrix)):
                if i != c:
                    col.append(matrix[i][r]);
            res = col.pop(0);
            for symbol in col:
                res = getSumWithXorInColumn(res,symbol);
            matrixTrans[r][c] = res;
    return matrixTrans;

def sumTwoMatrix(matrix1, matrix2):
    matrix = matrix1
    for r in range(len(matrix1)):
        for c in range(len(matrix2)):
            matrix[r][c] = getSumWithXorInColumn(matrix1[r][c],matrix2[r][c]);
    return matrix;

def getSumWithXorInColumn(a,b):
    res = int(binarySymbols[a],2)^int(binarySymbols[b],2);
    return getKeyFromDict(bin(res), binarySymbols);

def transpose(matrix):
    return [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix))];

def rotateLeft(l, n):
    return l[n:] + l[:n];

def rotateRight(l, n):
    return l[-n:] + l[:-n];

def getKeyFromDict(val, dictionary):
    for key, value in dictionary.items():
         if val == value:
             return key
    return None;

#def getFile(file): #Esta funcion quedo obsoleta.
#    try:           #Ahora la clave y el texto se leen de la entrada estandar.
#        fileObject = open(file, "r");#Y no desde archivos de texto.
#        data = fileObject.read();
#        return data;
#    except:
#       print("ERROR: No se encuentra el archivo: "+file);

def printMatrix(matrix):
    col = "";
    for r in range(len(matrix)):
        for c in range(len(matrix)):
            col += " " + matrix[c][r];
        col += "\n";
    print(col);

def getDimentions(text):
    size = math.sqrt(len(text));
    sizeParteEntera = int(math.sqrt(len(text)))
    while ((size % sizeParteEntera) != 0):
        if ((size - sizeParteEntera) < 1):
            return int(size) + 1;
        else:
            size += 1;
    return int(size);

def getRandomSymbol():
    return random.choice(list(dictionary.keys()));

def cipher(key, text):
    dimentions = getDimentions(text);
    if DEBUG:
        print("Obtenemos las dimensiones de la matriz.");
        print("Dimensiones: "+str(dimentions)+"x"+str(dimentions))
    while dimentions > int(math.sqrt(len(text))):
        text += getRandomSymbol();
    while dimentions > int(math.sqrt(len(key))):
        key += getRandomSymbol();
    print("Clave: ["+key+"]");
    print("Mensaje descifrado: ["+text+"]");
    matrix = getMatrix(dimentions, text);
    if DEBUG:
        print("Generamos la matriz de "+str(dimentions)+"x"+str(dimentions)+":");
        printMatrix(matrix);
    matrix = doSubstitution(matrix);
    if DEBUG:
        print("Aplicamos la substitución:");
        printMatrix(matrix);
    matrix = doCorrimientoFilasIzq(matrix);
    if DEBUG:
        print("Aplicamos el corrimiento de filas a la izquierda:");
        printMatrix(matrix);
    matrix = doTransformacionColumnas(matrix);
    if DEBUG:
        print("Aplicamos la transformación de columnas:");
        printMatrix(matrix);
    matrixkey = getMatrix(dimentions, key);
    if DEBUG:
        print("Generamos la matriz de la clave:");
        printMatrix(matrixkey);
    matrix = sumTwoMatrix(matrix, matrixkey);
    if DEBUG:
        print("Aplicamos la suma bit a bit de las 2 matrices anteriores:");
        printMatrix(matrix);
    cipher = "";
    for c in range(len(matrix)):
        for r in range(len(matrix)):
            cipher += matrix[c][r];
    print("Mensaje cifrado: ["+cipher+"]");
    return True;

def descipher(key, text):
    dimentions = getDimentions(text);
    if DEBUG:
        print("Obtenemos las dimensiones de la matriz.");
        print("Dimensiones: "+str(dimentions)+"x"+str(dimentions))
    print("Clave: ["+key+"]");
    print("Mensaje cifrado: ["+text+"]");
    dimentions = getDimentions(text);
    matrix = getMatrix(dimentions, text);
    if DEBUG:
        print("Generamos la matriz de n*n:");
        printMatrix(matrix);
    matrixkey = getMatrix(dimentions, key);
    if DEBUG:
        print("Generamos la matriz de la clave:");
        printMatrix(matrixkey);
    matrix = sumTwoMatrix(matrix, matrixkey);
    if DEBUG:
        print("Aplicamos la suma bit a bit de las 2 matrices anteriores:");
        printMatrix(matrix);
    matrix = doTransformacionColumnas(matrix);
    if DEBUG:
        print("Aplicamos la transformación de columnas:");
        printMatrix(matrix);
    matrix = doCorrimientoFilasDer(matrix);
    if DEBUG:
        print("Aplicamos el corrimiento de filas a la derecha:");
        printMatrix(matrix);
    matrix = doInverseSubstitution(matrix);
    if DEBUG:
        print("Aplicamos la substitución inversa:");
        printMatrix(matrix);
    descipher = "";
    for c in range(len(matrix)):
        for r in range(len(matrix)):
            descipher += matrix[c][r];
    print("Mensaje descifrado: ["+descipher+"]");
    return True;


menu();
option = input();
while (option != 'salir'):
    if (option == 'c'):
        if DEBUG:
            key = "BUGSLUCASCOYCSYP";
            text = "TICRIPTOVENCERAN";
        else:
            print("El largo del texto y el de la clave pueden ser diferentes... ;)")
            text = input("Ingresa texto a cifrar con SAES: ");
            key = input("Ingresa la clave: ");
        try:
            cipher(key, text);
            break;
        except:
            pass;
    elif (option == 'd'):
        if DEBUG:
            key = "BUGSLUCASCOYCSYP";
            text = "O?!LYPZ.JLÑNLNJO";
        else:
            text = input("Ingresa el texto a descifrar con SAES: ");
            key = input("Ingresa la clave: ");
        try:
            descipher(key, text);
            break;
        except:
            pass;
    elif (option == 'ci'):
        print("Función aún no implementada. :´(");
    elif (option == 'di'):
        print("Función aún no implementada. :´(");
    elif (option == 'salir'):
        break;
    menu();
    option = input();
