#!/usr/bin/python3
# -*- coding: utf-8 -*-

# CifradoCesar: El programa de cifrado y decifrado César.
# Copyright (C) 2021  free4fun (Mauricio Sosa Giri) <free4fun@riseup.net>
import re;

lmin = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z'];
lmay = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

def menu():
    print("Ingresa las letras 'cc' para cifrar con cifrado César.");
    print("Ingresa las letras 'dc' para descifrar con cifrado César.");
    print("Ingresa las letras 'dcf' para descifrar con cifrado César por fuerza bruta.");
    print("Ingresa la palabra 'salir' para cerrar el programa.");

def cipher(key, file):
    fileObject = open(file, "r");
    data = fileObject.read();
    res = "";
    key = int(key);
    for c in data:
        if (c in lmin):
            n = lmin.index(c);
            n = (n + key) % 27;
            d = lmin[n];
        elif (c in lmay):
            n = lmay.index(c);
            n = (n + 27 - key) % 27;
            d = lmay[n];
        else:
            d = c;
        res = res + d;
    print(res);

def descipher(key, file):
    fileObject = open(file, "r");
    data = fileObject.read();
    res = "";
    key = int(key);
    for c in data:
        if (c in lmin):
            n = lmin.index(c);
            n = (n + 27 - key) % 27;
            d = lmin[n];
        elif (c in lmay):
            n = lmay.index(c);
            n = (n + 27 - key) % 27;
            d = lmay[n];
        else:
            d = c;
        res = res + d;
    print("Clave cifrado: "+str(key));
    print(res);


print("El equipo de TICripto [BUGS] te da la bienvenida al programa de cifrado/descifrado César.");
menu();
option = input();
while (option != 'salir'):
    if (option == 'cc'):
        key = input("Ingresa el número secreto de descifrado:\n");
        file = input("Ingresa el nombre de archivo del texto o la ruta completa al mismo:\n");
        cipher(key, file);
        break;
    elif (option == 'dc'):
        key = input("Ingresa el número secreto de descifrado:\n");
        file = input("Ingresa el nombre de archivo del texto o la ruta completa al mismo:\n");
        descipher(key, file);
        break;
    elif (option == 'dcf'):
        file = input("Ingresa el nombre de archivo del texto o la ruta completa al mismo:\n");
        for key in range(27):
            descipher(key, file);
    menu();
    option = input();
